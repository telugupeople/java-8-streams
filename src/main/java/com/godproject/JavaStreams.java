package com.godproject;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JavaStreams {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] stringList = { "44", "467", "xyz" };

		List<String> list = Arrays.asList(stringList);

		//List<Integer> finalList = list.stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());

//		List<Integer> finalList = list.stream().map(Integer::parseInt).collect(Collectors.toList());
//		System.out.println("List of String ::" + finalList);
		//list.forEach(s->System.out.println(Integer.parseInt(s)));
//		list.forEach(s->{
//			try {
//				System.out.println(Integer.parseInt(s));
//			}catch(Exception ex){
//				System.out.println("Exception raised:" + ex.getMessage());
//			}
//			
//		});
		
		list.forEach(JavaStreams::printList);
			
	}

	private static void printList(String s) {
		try {
			System.out.println(Integer.parseInt(s));
		}catch(Exception ex) {
			System.out.println("Exception throw:: " + ex.getMessage());
		}
	}

}
